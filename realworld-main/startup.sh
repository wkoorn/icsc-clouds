#!/bin/sh

set -euo pipefail

./manage.py migrate
exec ./manage.py runserver 0.0.0.0:8080

